<?php

namespace Smtm\Frameless\Config;

/**
 * Class ConfigAggregator
 *
 * @package Smtm\Frameless\Config
 */
class ConfigAggregator
{
    /**
     * @var string
     */
    protected $configFile;
    /**
     * @var array
     */
    protected $config = [];

    /**
     * ConfigAggregator constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param $configFile
     * @return array
     */
    public function aggregate(string $configFile): array
    {
        $config          = [
            'dependencies' => [],
        ];
        $configProviders = [];
        if (is_file($configFile)) {
            $configProviders = include($configFile);
        }

        foreach ($configProviders as $configProviderClass) {
            $configProvider = new $configProviderClass;
            $config         = $this->mergeConfig($config, $configProvider());
        }

        return $config;
    }

    /**
     * Merges the configs together and takes multi-dimensional arrays into account.
     *
     * @param array $original
     * @param array $merging
     * @return array
     */
    protected function mergeConfig(array $original, array $merging)
    {
        $array = array_merge($original, $merging);
        foreach ($original as $key => $value) {
            if (!is_array($value)) {
                continue;
            }
            if (!array_key_exists($key, $merging)) {
                continue;
            }
            if (is_numeric($key)) {
                continue;
            }
            $array[$key] = $this->mergeConfig($value, $merging[$key]);
        }
        return $array;
    }
}
