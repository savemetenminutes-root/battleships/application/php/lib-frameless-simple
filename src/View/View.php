<?php

namespace Smtm\Frameless\View;

/**
 * Class View
 *
 * @package Smtm\Frameless\View
 */
class View implements ViewInterface
{
    /**
     * @var array
     */
    protected $config;
    /**
     * @var mixed
     */
    protected $layout;
    /**
     * @var mixed
     */
    protected $title;
    /**
     * @var
     */
    protected $headTemplate;
    /**
     * @var
     */
    protected $contentTemplate;
    /**
     * @var array
     */
    protected $variables = [];

    /**
     * View constructor.
     *
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->config = $config;
        $this->layout = $config['layout'] ?? $config['defaultLayout'];
        $this->title  = $config['title'] ?? $config['defaultTitle'];
        $this->head   = $config['head'] ?? '';
    }

    /**
     * @return false|string
     */
    public function render(): string
    {
        extract($this->variables);
        $title = $this->title;
        $head  = '';
        if ($this->getHeadTemplate() !== null) {
            ob_start();
            include($this->config['head'][$this->getHeadTemplate()]);
            $head = ob_get_clean();
        }
        $content = '';
        if ($this->getContentTemplate() !== null) {
            ob_start();
            include($this->config['content'][$this->getContentTemplate()]);
            $content = ob_get_clean();
        }
        ob_start();
        include($this->layout);
        return ob_get_clean();
    }

    /**
     * @return string
     */
    public function getHeadTemplate()
    {
        return $this->headTemplate;
    }

    /**
     * @param string $headTemplate
     * @return View
     */
    public function setHeadTemplate($headTemplate): View
    {
        $this->headTemplate = $headTemplate;
        return $this;
    }

    /**
     * @return string
     */
    public function getContentTemplate(): string
    {
        return $this->contentTemplate;
    }

    /**
     * @param string $contentTemplate
     * @return View
     */
    public function setContentTemplate(string $contentTemplate): View
    {
        $this->contentTemplate = $contentTemplate;
        return $this;
    }

    /**
     * @return string
     */
    public function getLayout(): string
    {
        return $this->layout;
    }

    /**
     * @param string $layout
     * @return View
     */
    public function setLayout(string $layout): View
    {
        $this->layout = $layout;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return View
     */
    public function setTitle(string $title): View
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return array
     */
    public function getVariables()
    {
        return $this->variables;
    }

    /**
     * @param array $variables
     * @return View
     */
    public function setVariables(array $variables): View
    {
        $this->variables = $variables;
        return $this;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getVariable(string $key)
    {
        return $this->variables[$key];
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return View
     */
    public function setVariable(string $key, $value): View
    {
        $this->variables[$key] = $value;
        return $this;
    }
}
