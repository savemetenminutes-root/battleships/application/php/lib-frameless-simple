<?php

namespace Smtm\Frameless\View;

/**
 * Class JsonView
 *
 * @package Smtm\Frameless\View
 */
class JsonView implements ViewInterface
{
    /**
     * @var array
     */
    protected $config;
    /**
     * @var array
     */
    protected $variables = [];

    /**
     * JsonView constructor.
     *
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->config = $config;
    }

    /**
     * @return string
     */
    public function render(): string
    {
        return json_encode($this->variables);
    }

    /**
     * @return array
     */
    public function getVariables()
    {
        return $this->variables;
    }

    /**
     * @param array $variables
     * @return JsonView
     */
    public function setVariables($variables)
    {
        $this->variables = $variables;
        return $this;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getVariable(string $key)
    {
        return $this->variables[$key];
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return JsonView
     */
    public function setVariable(string $key, $value): JsonView
    {
        $this->variables[$key] = $value;
        return $this;
    }
}
