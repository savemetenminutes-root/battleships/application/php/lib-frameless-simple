<?php

namespace Smtm\Frameless\View;

/**
 * Interface ViewInterface
 *
 * @package Smtm\Frameless\View
 */
interface ViewInterface
{
    /**
     * @return string
     */
    public function render(): string;
}
