<?php

namespace Smtm\Frameless\View\Factory;

use Smtm\Frameless\View\JsonView;

/**
 * Class JsonViewFactory
 *
 * @package Smtm\Frameless\View\Factory
 */
class JsonViewFactory
{
    /**
     * @param array $config
     * @param $request
     * @return JsonView
     */
    public function __invoke(array $config, $request)
    {
        $configTemplates = $config['templates'] ?? [];
        return new JsonView($configTemplates);
    }
}
