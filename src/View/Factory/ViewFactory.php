<?php

namespace Smtm\Frameless\View\Factory;

use Smtm\Frameless\View\View;

/**
 * Class ViewFactory
 *
 * @package Smtm\Frameless\View\Factory
 */
class ViewFactory
{
    /**
     * @param array $config
     * @param $request
     * @return View
     */
    public function __invoke(array $config, $request)
    {
        $configTemplates = $config['templates'] ?? [];
        return new View($configTemplates);
    }
}
