<?php

declare(strict_types=1);

namespace Smtm\Frameless;

/**
 * Class ConfigProvider
 *
 * @package Smtm\Frameless
 */
class ConfigProvider
{
    /**
     * @return array
     */
    public function __invoke(): array
    {
        return [
            'dependencies' => $this->getDependencies(),
        ];
    }

    /**
     * @return array
     */
    public function getDependencies(): array
    {
        return [
            \Smtm\Frameless\View\View::class     => \Smtm\Frameless\View\Factory\ViewFactory::class,
            \Smtm\Frameless\View\JsonView::class => \Smtm\Frameless\View\Factory\JsonViewFactory::class,
        ];
    }

    /**
     * @return array
     */
    public function getTemplates(): array
    {
        return [
            'defaultLayout' => __DIR__ . '/../templates/layout.phtml',
            'defaultTitle'  => 'SaveMeTenMinutes - The Frameless Framework',
        ];
    }
}
